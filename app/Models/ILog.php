<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ILog extends BaseModel
{

    protected $guarded = ['id'];

    protected $ins_name = 'log';
}
