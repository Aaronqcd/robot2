<?php
return [
    'public_ins' =>
        [
            'auth',
            'init',
            'location',
        ],

    'department' =>
        [
            'department_class1' =>
                [
                    'a1',
                    'a2',
                ],
            'department_class2' =>
                [
                    'a1',
                    'a2',
                ],
            'mark' =>
                [
                    'r_',
                ],
            'hospital' =>
                [
                    'r',
                ],
            'agency' =>
                [
                    'r',
                ],
        ],
    'agency' =>
        [
            'mark' =>
                [
                    'r_',
                    'bat_cu',
                    'cu_',
                    'cu',
                    'bat_exists',
                ],
            'hospital' =>
                [
                    'r',
                    'b2',
                ],
            'agency' =>
                [
                    'r',
                ],
            'doctor' =>
                [
                    'r',
                ],
        ],
    'doctor' =>
        [
            'mark'
            =>
                [
                    'scan_u'
                ]
        ]
];