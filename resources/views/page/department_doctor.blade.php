<section id="main-content">
    <!--tiles start-->
    <div class="row">
        <div class="col-md-12">
            @include('page.department')
            @include('page.doctor')
        </div>
    </div>
</section>