<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Robot</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <link rel="shortcut icon" href="{{base_url()}}assets/img/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="{{base_url()}}vendor/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Icons -->
    <link rel="stylesheet" href="{{base_url()}}assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{base_url()}}vendor/toastr/toastr.min.css">
    <link rel="stylesheet" href="{{base_url()}}assets/css/animate.css">
    <link rel="stylesheet" href="{{base_url()}}assets/css/main.css">

    <link rel="stylesheet" href="{{base_url()}}css/base.css">
    <link rel="stylesheet" href="{{base_url()}}vendor/ng-dialog/css/ngDialog.css">
    <link rel="stylesheet" href="{{base_url()}}vendor/ng-dialog/css/ngDialog-theme-default.css">
      <!-- style sheet -->
    <link rel="stylesheet" href="{{base_url()}}vendor/angular-material/angular-material.css">

</head>