<!--main content end-->
<!--Global JS-->
<script type="text/javascript" src="{{base_url() . 'vendor/jquery/dist/jquery.js'}}"></script>
<script type="text/javascript" src="{{base_url() . 'vendor/angular/angular.js'}}"></script>
<script type="text/javascript" src="{{base_url() . 'vendor/nprogress/nprogress.js'}}"></script>
<script type="text/javascript" src="{{base_url() . 'vendor/toastr/toastr.js'}}"></script>
<script type="text/javascript" src="{{base_url() . 'vendor/bootstrap/dist/js/bootstrap.js'}}"></script>
<script type="text/javascript" src="{{base_url() . 'vendor/moment/min/moment.min.js'}}"></script>
<script type="text/javascript" src="{{base_url() . 'vendor/angular-ui-router/release/angular-ui-router.js'}}"></script>
<script type="text/javascript" src="{{base_url() . 'vendor/angular-bootstrap/ui-bootstrap-tpls.js'}}"></script>
<script type="text/javascript" src="{{base_url() . 'vendor/angular-filter/dist/angular-filter.js'}}"></script>
<script type="text/javascript" src="{{base_url() . 'vendor/ng-dialog/js/ngDialog.js'}}"></script>
<script type="text/javascript" src="{{base_url() . 'vendor/ng-file-upload/ng-file-upload-shim.min.js'}}"></script>
<script type="text/javascript" src="{{base_url() . 'vendor/ng-file-upload/ng-file-upload.min.js'}}"></script>
<script type="text/javascript" src="{{base_url() . 'vendor/angular-animate/angular-animate.js'}}"></script>
<script type="text/javascript" src="{{base_url() . 'vendor/angular-aria/angular-aria.js'}}"></script>
<script type="text/javascript" src="{{base_url() . 'vendor/angular-material/angular-material.js'}}"></script>

<script type="text/javascript" src="{{ base_url() . 'univ_js/directive/d.js'}}"></script>
<script type="text/javascript" src="{{ base_url() . 'univ_js/service/s.js'}}"></script>

<script type="text/javascript" src="{{ base_url() . 'univ_js/h.js'}}"></script>

<script type="text/javascript" src="{{ base_url() . 'js/service/s.js'}}"></script>
<script type="text/javascript" src="{{ base_url() . 'js/service/agency.js'}}"></script>
<script type="text/javascript" src="{{ base_url() . 'js/service/base.js'}}"></script>
<script type="text/javascript" src="{{ base_url() . 'js/service/department.js'}}"></script>
<script type="text/javascript" src="{{ base_url() . 'js/service/doctor.js'}}"></script>
<script type="text/javascript" src="{{ base_url() . 'js/service/h.js'}}"></script>
<script type="text/javascript" src="{{ base_url() . 'js/service/hospital.js'}}"></script>
<script type="text/javascript" src="{{ base_url() . 'js/filter/f.js'}}"></script>
<script type="text/javascript" src="{{ base_url() . 'js/directive/d.js'}}"></script>
<script type="text/javascript" src="{{ base_url() . 'js/router/r.js'}}"></script>
<script type="text/javascript" src="{{ base_url() . 'js/controller/c.js'}}"></script>
<script type="text/javascript" src="{{ base_url() . 'js/base.js'}}"></script>

<!--Load these page level functions-->

</body>

</html>